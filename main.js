
var api__key = "72b56103e43843412a992a8d64bf96e9";
var language_setting = "ru-RU";

$(document).ready(function(){

    var desc_div = "<div class='card-deck'>";
    var desc_div_end = "</div>";
    $.getJSON(
        "https://api.themoviedb.org/3/movie/popular",
        {
            api_key: api__key,
            language: language_setting
        },
        function( response ) {   
            var content = "";
            $.each(response['results'], function(key, value) {
                content += "<li>";
                content += "<div id='"+value.id+"' class='card film-card'><img class='card-img-top film-poster' src='https://image.tmdb.org/t/p/w300"+value.poster_path+"'>";
                content += "<div class='card-block film-description'><h4 class='card-title film-title'>"+value.title+"</h4>";
                content += "<p class='card-text film-overview'>"+value.overview+"</p>";
                content += "<p class='card-text film-release-date'><small class='text-muted'>"+value.release_date+"</small></p></div></div>";
                content += "</li>";
                $(content).appendTo('#main-block');
                content ="";                                   
            });
        }
    );

});

$('#searchButton').click(function(){
    var searchQuery = $('#sQuery').val();

    var desc_div = "<div class='card-deck'>";
    var desc_div_end = "</div>";
    $.getJSON(
        "https://api.themoviedb.org/3/search/movie",
        {
            api_key: api__key,
            language: language_setting,
            query: searchQuery
        },
        function( response ) {
            
            var content = "";
            var i = 0;
            var temp = 0;
            $('#main-block').html("");
            $.each(response['results'], function(key, value) {
                                                
                if((i%3) == 0) content += desc_div;
                content += "<div class='col-lg-4 col-md-6'>";
                content += "<div id='"+value.id+"' class='card film-card'><img class='card-img-top film-poster' src='https://image.tmdb.org/t/p/w300"+value.poster_path+"'>";
                content += "<div class='card-block film-description'><h4 class='card-title film-title'>"+value.title+"</h4>";
                //content += "<p class='card-text film-overview'>"+value.overview+"</p>";
                content += "<p class='card-text film-release-date'><small class='text-muted'>"+value.release_date+"</small></p></div></div>";
                content += "</div>";
                //if(i%3) content += desc_div_end;

                //$(content).appendTo('#main-block');
                temp++;
                i++;
                if( temp == 3 ){
                    $(content).appendTo('#main-block');
                    content ="";
                    temp = 0;                    
                }
                if((response['results'].length-i) < 3){
                    $(content).appendTo('#main-block');
                    content ="";
                    temp = 0;
                }
                //content ="";
                 
            })
        }
    );

    return false;
});


